import 'jquery.global.js';
//import 'lazysizes';

import fancybox from '@fancyapps/fancybox';
import page from 'page';
import forms from 'forms';
import {
  disableBodyScroll,
  enableBodyScroll,
  clearAllBodyScrollLocks
} from 'body-scroll-lock';


import mainPage from 'index.page';
import ymap from 'ymap';


let app = {

  scrollToOffset: 200, // оффсет при скролле до элемента
  scrollToSpeed: 500, // скорость скролла

  init: function() {
    // read config
    if (typeof appConfig === 'object') {
      Object.keys(appConfig).forEach(key => {
        if (Object.prototype.hasOwnProperty.call(app, key)) {
          app[key] = appConfig[key];
        }
      });
    }

    app.currentID = 0;

    // Init page
    this.page = page;
    this.page.init.call(this);

    this.ymap = ymap;
    this.ymap.init.call(this);

    this.forms = forms;
    this.forms.init.call(this);

    // Init page

    this.mainPage = mainPage;
    this.mainPage.init.call(this);

    //window.jQuery = $;
    window.app = app;

    app.document.ready(() => {

      $('[data-fancybox], .js-modal').fancybox({
        toolbar: false,
        smallBtn: true,
        touch: false,
        autoFocus: false,

        afterLoad: function() {
          var fancyboxSlide = document.querySelectorAll(".fancybox-slide");
          fancyboxSlide.forEach(function(element) {
            // scrollLock.disablePageScroll(element);
            disableBodyScroll(element);

          });
        },
        beforeClose: function() {

          if ($('.fancybox-slide').length == 1) {
            // scrollLock.enablePageScroll();
            clearAllBodyScrollLocks();
          }
        },
      });

      $('.js-video').fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
          media: {},
        },
      });

      $(window).on('resize', function() {
        changeTextSales();
      });

      function changeTextSales() {
        if ($(window).width() <= 680) {
          $('.sales-js-text').text('Наши клиенты продают недвижимость на электронных торгах в среднем');
        }
      }

      changeTextSales();



    });

    app.window.on('load', () => {
      let selectSingle = document.querySelectorAll('.js-select');
      selectSingle.forEach(function(select) {
        choiceOption(select);
      })

      function choiceOption(selectSingle) {

        let selectSingle_title = selectSingle.querySelector('.js-title');
        let selectSingle_labels = selectSingle.querySelectorAll('.js-label');
        // Toggle menu
        selectSingle_title.addEventListener('click', () => {
          if ('active' === selectSingle.getAttribute('data-state')) {
            selectSingle.setAttribute('data-state', '');
          } else {
            selectSingle.setAttribute('data-state', 'active');
          }
        });

        // Close when click to option
        for (let i = 0; i < selectSingle_labels.length; i++) {
          selectSingle_labels[i].addEventListener('click', (evt) => {
            selectSingle_title.textContent = evt.target.textContent;
            selectSingle.setAttribute('data-state', '');
          });
        }
      }


    });

    // this.document.on(app.resizeEventName, () => {
    // });

  },

};
app.init();