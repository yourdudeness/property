import 'jquery.global.js';
import slick from 'slick-carousel';

let mainPage = {

  init: function() {
    mainPage.app = this;

    this.document.ready(() => {
      mainPage.initIndex();
    });
  },

  initIndex() {
    $('.js-tabs-item').on('click', function(e) {
      e.preventDefault();
      const tabs = $(this).closest('.js-tabs');
      const tabID = $(this).attr('data-tab');

      $(this).closest('.js-tabs').find('.js-tab-content').removeClass('active');
      $('#' + tabID).addClass('active');

      tabs.find('.js-tabs-item').removeClass('active');
      $(this).addClass('active');
    });

    $('.js-step-slider').slick({
      dots: false,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
    });

    $(window).on('resize', function() {
      initApartmentSlider();
      initWorthSlider();
      initFaqSlider();
      changeText();
    });

    function initApartmentSlider() {
      if ($(window).width() <= 680) {
        $("div.first-slider").remove();
        $('.js-apartment-slider').slick({
          dots: false,
          infinite: false,
          slidesToShow: 2.5,
          slidesToScroll: 1,
          arrows: true,

          responsive: [{
              breakpoint: 550,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 481,
              settings: {
                slidesToShow: 1.7,
                slidesToScroll: 1,
              }
            },

            {
              breakpoint: 341,
              settings: {
                slidesToShow: 1.4,
                slidesToScroll: 1,
              }
            }

          ]
        });
      }
    }

    function initWorthSlider() {
      if ($(window).width() <= 680) {
        $('.js-worth-list-slider').slick({
          dots: false,
          infinite: false,
          slidesToShow: 2.5,
          slidesToScroll: 1,
          arrows: true,
          responsive: [{
              breakpoint: 550,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 481,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              }
            }

          ]
        });
      }
    }

    function initFaqSlider() {
      if ($(window).width() <= 550) {
        $('.js-faq-list').slick({
          dots: false,
          infinite: false,
          slidesToShow: 1.6,
          slidesToScroll: 1,
          arrows: true,
          responsive: [{
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              }
            },

          ]
        });
      }
    }

    function changeText() {
      if ($(window).width() <= 480) {
        $('.status-work p').text('online');
        $('.time-work-status-item').text('online');
      }
    }

    initApartmentSlider();
    initWorthSlider();
    initFaqSlider();
    changeText();
  }
};

export default mainPage;