import $ from 'jquery';

let ymap = {

  init: function() {
    ymap.app = this;

    this.document.ready(() => {
      ymap.initMap();
    });
  },

  initMap() {
    setTimeout(function() {
      if ($('#map').length) {
        var map = document.querySelector('#map');

        var onIntersection = function(entries) {
          for (const entry of entries) {
            if (entry.intersectionRatio > 0) {
              ymaps.ready(mapsInit);
              observer.unobserve(entry.target);
            }
          }
        };

        var observer = new IntersectionObserver(onIntersection);
        observer.observe(map);
        let center = [55.75675727558734, 37.55471908281924];
        let zoom = 17;
        let iconImageSize = [148, 150];

        if ($(window).width() <= '480') {
          center = [55.75703202276608, 37.54934123677064];
          zoom = 16;
          iconImageSize = [48, 50];
        } else if ($(window).width() <= '768') {
          center = [55.7569231219585, 37.54964164418029];
          
        } else if ($(window).width() <= '899') {
          center = [55.75669408452474,37.55156682009886];
        } else if ($(window).width() <= '1080') {
          center = [55.7566396336853,37.54963562960814];
        } else {
          center = [55.75675727558734, 37.55471908281924];
        }

        function mapsInit() {
          var myMap = new ymaps.Map('map', {
            center,
            zoom,
            controls: ['zoomControl'],
            iconImageSize,
          });
          var Nizhniy = new ymaps.Placemark(
            [55.756698287270495, 37.549601428018825], {
              hintContent: '',
              iconContent: ''
            }, {
              iconLayout: 'default#image',
              iconImageHref: '/assets/img/mark.png',
              iconImageSize,
              iconImageOffset: [-72, -77],
            },
          );
          myMap.geoObjects.add(Nizhniy);
          myMap.behaviors.disable('scrollZoom');
        }
      }
    }, 1000);
  }


};

export default ymap;